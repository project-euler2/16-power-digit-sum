def power_digit_sum(number, power):
    power_digit = number ** power
    str_power_digit = str(power_digit)
    sum = 0
    for num in str_power_digit:
        sum += int(num)
    return sum

print(power_digit_sum(2, 1000))